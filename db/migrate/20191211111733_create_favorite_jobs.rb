class CreateFavoriteJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :favorite_jobs, id: false do |t|
      t.text     :title, null: false
      t.timestamps
    end

    add_index :favorite_jobs, :title, unique: true
  end
end
