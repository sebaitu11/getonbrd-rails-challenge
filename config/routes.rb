Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # 
  resources :favorite_jobs do 
    member do
      post :unfavorite
    end

    collection do 
      post :favorite
    end
  end

  root 'welcome#index'

end
