class FavoriteJobsController < ApplicationController
  

  def favorite
    @favorite_job = FavoriteJob.new(title: params[:title])
    if @favorite_job.save
      render json: { success: true, job: @favorite_job }
    else
      render json: {success: false}
    end
  end

  def unfavorite
    @favorite_job = FavoriteJob.find(params[:id])
    if @favorite_job.destroy
      render json: { success: true, job: @favorite_job }
    else
      render json: {success: false}
    end
  end

  def index
    @favorite_jobs = FavoriteJob.all
    render json: {favorites: @favorite_jobs}
  end
  

end